function tiempo = bhe2pl(img_path, indice)
    tic
    img = imread(img_path); %lee la imagen  
    h = (imhist(img))'; %extraemos el histograma de la imagen
    [m,n] = size(img); %obtiene las dimensiones de imagen
    p = h ./ (m*n); %calcula funcion de probabilidad    
    i = 1:256;
    sp = round(sum(p.*i));%calcula sp (intensidad media global)

    hl = h(1:sp); %inicializa sub-histograma inferior
    nl = sum(hl); %area del sub-histograma inferior
    hu = h(sp+1:256); %inicializa sub-histograma superior
    nu = sum(hu); %area del sub histograma superior
    pkl = max(hl); %pico del sub-histograma inferior
    pku = max(hu); %pico del sub-histograma superior
    
    probinf = hl ./ nl; %calcula funcion de probabilidad del sub-histograma inferior
    probsup = hu ./ nu; %calcula funcion de probabilidad del sub-histograma superior
    
    i = 1:size(probinf, 2);
    spl = round(sum(probinf.*i)); %calcula spl (intensidad media inferior)
    
    i = 1:size(probsup, 2);
    spu = round(sum(probsup.*i)); %calcula su (intensidad media superior)
    
    imin = find(h,1,'first'); %halla imin
    imax = find(h,1,'last'); %halla imax
    
    grl1 = (sp - spl)/(sp - imin); %calcula grl1
    gru1 = (imax - spu)/(imax - sp); %calcula gru1
    
    if grl1 > 0.5
        dl = (1-grl1)/2;
    else                %calcula dl
        dl = grl1/2;
    end
    
    if gru1 > 0.5
        du = (1-gru1)/2;
    else                %calcula du
        du = gru1/2;
    end
    
    grl2 = grl1 + dl; %calcula grl2
    gru2 = gru1 + du; %calcula gru2
    
    pll1 = grl1 * pkl; %calcula pll1
    pll2 = grl2 * pkl; %calcula pll2
    plu1 = gru1 * pku; %calcula plu1
    plu2 = gru2 * pku; %calcula plu2
   
    mhl = ((hl > pll2)*pll2) + ((hl <= pll2)*pll1); %modifica hl   
    mhu = ((hu > plu2)*plu2) + ((hu <= plu2)*plu1); %modifica hu  

    probinf = mhl ./ nl; %actualiza probinf
    cl = cumsum(probinf); %calcula cl
    probsup = mhu ./ nu; %actualiza probsub
    cu = cumsum(probsup); %calcula cu
    
    ft1 = imin + (sp - imin)*(cl-0.5*probinf);
    ft2 = sp+1 + (imax - sp - 1)*(cu-0.5*probsup);
    fteto = [ft1, ft2]; %realiza transformaciones sobre los sub-histogramas
    
    newimg = fteto(img +1); %obtiene imagen mejorada

    %imshow(uint8(newimg))
    %figure, imshow(img);
    t1 = toc;
    %imwrite(uint8(newimg), strcat(num2str(indice),'-2pl.tiff'));
    tiempo = t1;
end
