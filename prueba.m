function [] = prueba()
    %{
    %TIEMPOS DE EJECUCION
    %HEB2PL
    j=1;
    for i = 51:191
        tiempos2pld1(j) = bhe2pl(strcat(num2str(i),'.tiff'), i);
        j = j + 1;
    end
    disp(tiempos2pld1);
    maximo2pld1 = max(tiempos2pld1)
    promedio2pld1 = mean(tiempos2pld1)
    minimo2pld1 = min(tiempos2pld1)
    
    for i = 1:50
        tiempos2pld2(i) = bhe2pl(strcat(num2str(i),'.tiff'), i);
    end
    disp(tiempos2pld2);
    maximo2pld2 = max(tiempos2pld2)
    promedio2pld2 = mean(tiempos2pld2)
    minimo2pld2 = min(tiempos2pld2)
    
    %HEB3PL
    j = 1;
    for i = 51:191
        tiempos3pld1(j) = bhe3pl(strcat(num2str(i),'.tiff'), i);
        j = j+ 1;
    end
    disp(tiempos3pld1);
    maximo3pld1 = max(tiempos3pld1)
    promedio3pld1 = mean(tiempos3pld1)
    minimo3pld1 = min(tiempos3pld1)
    
    for i = 1:50
        tiempos3pld2(i) = bhe3pl(strcat(num2str(i),'.tiff'), i);
    end
    disp(tiempos2pld2);
    maximo3pld2 = max(tiempos3pld2)
    promedio3pld2 = mean(tiempos3pld2)
    minimo3pld2 = min(tiempos3pld2)
    %}
    %{
    %AMBE
    j=1;
    for i = 51:191
        ambe2pld1(j) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-2pl.tiff'));
        j = j + 1;
    end
    disp(ambe2pld1);
    maximo2pld1 = max(ambe2pld1)
    promedio2pld1 = mean(ambe2pld1)
    minimo2pld1 = min(ambe2pld1)
    
    for i = 1:50
        ambe2pld2(i) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-2pl.tiff'));
    end
    disp(ambe2pld2);
    maximo2pld2 = max(ambe2pld2)
    promedio2pld2 = mean(ambe2pld2)
    minimo2pld2 = min(ambe2pld2)
    
    %HEB3PL
    j = 1;
    for i = 51:191
        ambe3pld1(j) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-3pl.tiff'));
        j = j+ 1;
    end
    disp(ambe3pld1);
    maximo3pld1 = max(ambe3pld1)
    promedio3pld1 = mean(ambe3pld1)
    minimo3pld1 = min(ambe3pld1)
    
    for i = 1:50
        ambe3pld2(i) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-3pl.tiff'));
    end
    disp(ambe2pld2);
    maximo3pld2 = max(ambe3pld2)
    promedio3pld2 = mean(ambe3pld2)
    minimo3pld2 = min(ambe3pld2)
    %}
    %{
    %PSNR
    j=1;
    for i = 51:191
        psnr2pld1(j) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-2pl.tiff'));
        j = j + 1;
    end
    disp(psnr2pld1);
    maximo2pld1 = max(psnr2pld1)
    promedio2pld1 = mean(psnr2pld1)
    minimo2pld1 = min(psnr2pld1)
    
    for i = 1:50
        psnr2pld2(i) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-2pl.tiff'));
    end
    disp(psnr2pld2);
    maximo2pld2 = max(psnr2pld2)
    promedio2pld2 = mean(psnr2pld2)
    minimo2pld2 = min(psnr2pld2)
    
    %HEB3PL
    j = 1;
    for i = 51:191
        psnr3pld1(j) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-3pl.tiff'));
        j = j+ 1;
    end
    disp(psnr3pld1);
    maximo3pld1 = max(psnr3pld1)
    promedio3pld1 = mean(psnr3pld1)
    minimo3pld1 = min(psnr3pld1)
    
    for i = 1:50
        psnr3pld2(i) = medir(strcat(num2str(i),'.tiff'), strcat(num2str(i),'-3pl.tiff'));
    end
    disp(psnr2pld2);
    maximo3pld2 = max(psnr3pld2)
    promedio3pld2 = mean(psnr3pld2)
    minimo3pld2 = min(psnr3pld2)
    
    %ENTROPIA   
    %HEB2PL
    j=1;
    for i = 51:191
        A = imread(strcat(num2str(i),'-2pl.tiff'));
        entr2pld1(j) = entropy(A); %2pl d1
        j = j + 1;
    end
    csvwrite('entropy-bhe2pl-d1',entr2pld1);
    disp(entr2pld1);
    maximo2pld1 = max(entr2pld1)
    promedio2pld1 = mean(entr2pld1)
    minimo2pld1 = min(entr2pld1)
    
    for i = 1:50
        A = imread(strcat(num2str(i),'-2pl.tiff'));
        entr2pld2(i) = entropy(A); %2pl d2
    end
    csvwrite('entropy-bhe2pl-d2',entr2pld2);
    disp(entr2pld2);
    maximo2pld2 = max(entr2pld2)
    promedio2pld2 = mean(entr2pld2)
    minimo2pld2 = min(entr2pld2)
    
    %HEB3PL
    j = 1;
    for i = 51:191
        A = imread(strcat(num2str(i),'-3pl.tiff'));
        entr3pld1(j) = entropy(A); %3pl d1
        j = j+ 1;
    end
    csvwrite('entropy-bhe3pl-d1',entr3pld1);
    disp(entr3pld1);
    maximo3pld1 = max(entr3pld1)
    promedio3pld1 = mean(entr3pld1)
    minimo3pld1 = min(entr3pld1)
    
    for i = 1:50
        A = imread(strcat(num2str(i),'-3pl.tiff'));
        entr3pld2(i) = entropy(A); %3pl d2
    end
    csvwrite('entropy-bhe3pl-d2',entr3pld2);
    disp(entr2pld2);
    maximo3pld2 = max(entr3pld2)
    promedio3pld2 = mean(entr3pld2)
    minimo3pld2 = min(entr3pld2)
    %}
    %{
    %CONTRASTE
    %HEB2PL
    j=1;
    for i = 51:191
        A = imread(strcat(num2str(i),'-2pl.tiff'));
        h1 = (imhist(A))'; %extrae histograma 1   
        [m1,n1] = size(A); %obtiene las dimensiones de imagen original
        p1 = h1 ./ (m1*n1); %calcula funcion de probabilidad de la imagen original

        i = 1:size(h1,2);
        spl = round(sum(p1.*i));%calcula sp (intensidad media global)

        contraste1 = 0;
        for i = 1:size(p1,2) % CALCULA CONSTRASTE DE IMAGEN ORIGINAL
            contraste1 = contraste1 + ((i - spl)^2)*p1(i);
        end
        contraste1 = sqrt(contraste1);
        
        contraste2pld1(j) = contraste1; %2pl d1
        j = j + 1;
    end
    csvwrite('contraste-bhe2pl-d1',contraste2pld1);
    disp(contraste2pld1);
    maximo2pld1 = max(contraste2pld1)
    promedio2pld1 = mean(contraste2pld1)
    minimo2pld1 = min(contraste2pld1)
    j =1;
    for i = 1:50
        A = imread(strcat(num2str(i),'-2pl.tiff'));
        h1 = (imhist(A))'; %extrae histograma 1   
        [m1,n1] = size(A); %obtiene las dimensiones de imagen original
        p1 = h1 ./ (m1*n1); %calcula funcion de probabilidad de la imagen original

        i = 1:size(h1,2);
        spl = round(sum(p1.*i));%calcula sp (intensidad media global)

        contraste1 = 3;
        for i = 1:size(p1,2) % CALCULA CONSTRASTE DE IMAGEN ORIGINAL
            contraste1 = contraste1 + ((i - spl)^2)*p1(i);
        end
        contraste1 = sqrt(contraste1);
        
        contraste2pld2(j) = contraste1; %2pl d2
        
        j = j +1;
    end
    csvwrite('contraste-bhe2pl-d2',contraste2pld2);
    disp(contraste2pld2);
    maximo2pld2 = max(contraste2pld2)
    promedio2pld2 = mean(contraste2pld2)
    minimo2pld2 = min(contraste2pld2)
    
    %HEB3PL
    j = 1;
    for i = 51:191
        A = imread(strcat(num2str(i),'-3pl.tiff'));
         h1 = (imhist(A))'; %extrae histograma 1   
        [m1,n1] = size(A); %obtiene las dimensiones de imagen original
        p1 = h1 ./ (m1*n1); %calcula funcion de probabilidad de la imagen original

        i = 1:size(h1,2);
        spl = round(sum(p1.*i));%calcula sp (intensidad media global)

        contraste1 = 0;
        for i = 1:size(p1,2) % CALCULA CONSTRASTE DE IMAGEN ORIGINAL
            contraste1 = contraste1 + ((i - spl)^2)*p1(i);
        end
        contraste1 = sqrt(contraste1);
        
        contraste3pld1(j) = contraste1; %3pl d1
        
        j = j+ 1;
    end
    csvwrite('contraste-bhe3pl-d1',contraste3pld1);
    disp(contraste3pld1);
    maximo3pld1 = max(contraste3pld1)
    promedio3pld1 = mean(contraste3pld1)
    minimo3pld1 = min(contraste3pld1)
    j = 1;
    for i = 1:50
        A = imread(strcat(num2str(i),'-3pl.tiff'));
         h1 = (imhist(A))'; %extrae histograma 1   
        [m1,n1] = size(A); %obtiene las dimensiones de imagen original
        p1 = h1 ./ (m1*n1); %calcula funcion de probabilidad de la imagen original

        i = 1:size(h1,2);
        spl = round(sum(p1.*i));%calcula sp (intensidad media global)

        contraste1 = 0;
        for i = 1:size(p1,2) % CALCULA CONSTRASTE DE IMAGEN ORIGINAL
            contraste1 = contraste1 + ((i - spl)^2)*p1(i);
        end
        contraste1 = sqrt(contraste1);
        contraste3pld2(j) = contraste1;%2pl d2
        j = j +1;
    end
    csvwrite('contraste-bhe3pl-d2',contraste3pld2);
    disp(contraste3pld2);
    maximo3pld2 = max(contraste3pld2)
    promedio3pld2 = mean(contraste3pld2)
    minimo3pld2 = min(contraste3pld2)
    %}
end