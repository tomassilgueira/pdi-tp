function metrica = medir(img1_path, img2_path)
    img1 = imread(img1_path); %lee la imagen original
    img2 = imread(img2_path); %lee la imagen resultante
    
    %{
    %AMBE
    h1 = (imhist(img1))'; %extrae histograma 1
    h2 = (imhist(img2))'; %extrae histograma 2
    

    
    [m1,n1] = size(img1); %obtiene las dimensiones de imagen original
    [m2,n2] = size(img2); %obtiene las dimensiones de imagen resultante

    p1 = h1 ./ (m1*n1); %calcula funcion de probabilidad de la imagen original
    p2 = h2 ./ (m2*n2); %calcula funcion de probabilidad de la imagen resultante
    
    
    sp1 = 0;
    for i = 1:size(h1,2) %calcula la intensidad media de la imagen original
        sp1 = sp1 + p1(i)*i;
    end
    
    sp2 = 0;
    for i = 1:size(h2,2) %calcula la intensidad media de la imagen resultante
        sp2 = sp2 + p2(i)*i;
    end
    
    ambe = abs(sp1 - sp2); %ERROR DE LUMINOSIDAD MEDIA ABSOLUTO (AMBE)
    metrica = ambe;
    %}
    %PSNR
    peaksnr = psnr(img2,img1);
    metrica = peaksnr;
    %{      
    
    contraste1 = 0;
    for i = 1:size(p1,2) % CALCULA CONSTRASTE DE IMAGEN ORIGINAL
        contraste1 = contraste1 + ((i - sp1)^2)*p1(i);
    end
    constraste1 = sqrt(contraste1);

    
    contraste2 = 0;
    for i = 1:size(p2,2) % CALCULA CONSTRASTE DE IMAGEN RESULTANTE
        contraste2 = contraste2 + ((i - sp2)^2)*p2(i);
    end
    constraste2 = sqrt(contraste2);
    %}
end