function tiempo = bhe3pl(img_path, indice)
    tic;
    img = imread(img_path); %lee la imagen
    h = (imhist(img))'; %extraemos el histograma de la imagen
    [m,n] = size(img); %obtiene las dimensiones de imagen
    p = h ./ (m*n); %calcula funcion de probabilidad
    i = 1:256;
    sp = round(sum(p.*i));%calcula sp (intensidad media global)

    hl = h(1:sp); %inicializa sub-histograma inferior
    nl = sum(hl); %area del sub-histograma inferior
    hu = h(sp+1:256); %inicializa sub-histograma superior
    nu = sum(hu); %area del sub histograma superior
    pkl = max(hl); %pico del sub-histograma inferior
    pku = max(hu); %pico del sub-histograma superior
    
    probinf = hl ./ (nl); %calcula funcion de probabilidad del sub-histograma inferior
    probsup = hu ./ (nu); %calcula funcion de probabilidad del sub-histograma superior
    
    i = 1:size(probinf, 2);
    spl = round(sum(probinf.*i)); %calcula spl (intensidad media inferior)
    
    i = 1:size(probsup, 2);
    spu = round(sum(probsup.*i)); %calcula su (intensidad media superior)
    
    imin = find(h,1,'first'); %halla imin   
    imax = find(h,1,'last'); %halla imax
    
    grl2 = (sp - spl)/(sp - imin); %calcula grl2
    gru2 = (imax - spu)/(imax - sp); %calcula gru2
    
    if grl2 > 0.5
        dl = (1-grl2)/2;
    else                %calcula dl
        dl = grl2/2;    
    end
    
    if gru2 > 0.5
        du = (1-gru2)/2;
    else                %calcula du
        du = gru2/2;
    end
    
    grl1 = grl2 - dl; %calcula grl1
    grl3 = grl2 + dl; %calcula grl3
    gru1 = gru2 - du; %calcula gru1
    gru3 = gru2 + du; %calcula gru3
    
    pll1 = grl1 * pkl; %calcula pll1
    pll2 = grl2 * pkl; %calcula pll2
    pll3 = grl3 * pkl; %calcula pll3
    plu1 = gru1 * pku; %calcula plu1
    plu2 = gru2 * pku; %calcula plu2
    plu3 = gru3 * pku; %calcula plu3
    
    mhl = ((hl <= pll1)*pll1)+ ((hl > pll1 & hl <= pll3)*pll2) + ((hl > pll3)*pll3); %modifica hl    
    mhu = ((hu <= plu1)*plu1) + ((hu > plu1 & hu <= plu3)*plu2) + ((hu > plu3)*plu3); %modifica hu
    probinf = mhl ./ nl; %actualiza funcion de probabilidad del sub-histograma inferior
    cl = cumsum(probinf); %calcula funcion de densidad cumulativa del sub-histograma inferior
    probsup = mhu ./ nu; %actualiza funcion de probabilidad del sub-histograma superior
    cu = cumsum(probsup); %calcula funcion de densidad cumulativa del sub-histograma superior

    ft1 = sp*cl;
    ft2 = sp + 1 + (255 - sp - 1)*(cu);
    fteto = [ft1, ft2]; %realiza transformaciones sobre los sub-histogramas
    newimg = fteto(img +1); %obtiene imagen mejorada
 
    %imshow(uint8(newimg))
    %figure, imshow(img);
    %t2 = toc;
    %imwrite(newimg, strcat(num2str(indice),'-3pl.tiff'));
    %tiempo = t2;
end